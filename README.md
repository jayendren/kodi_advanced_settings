## Usage

copy file to $KODI_HOME/userdata/advancedsettings.xml

## KODI_HOME/userdata location

```
Operating system	File path
Android				Android/data/org.xbmc.kodi/files/.kodi/userdata/ (see note)
iOS					/private/var/mobile/Library/Preferences/Kodi/userdata/
Linux				~/.kodi/userdata/
Mac					/Users/<your_user_name>/Library/Application Support/Kodi/userdata/ (see note)
OpenELEC			/storage/.kodi/userdata/
Windows				Start - type %APPDATA%\kodi\userdata - press <Enter>
```

## Resources

see http://kodi.wiki/view/Advancedsettings.xml